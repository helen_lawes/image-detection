import React from 'react';
import { shallow } from 'enzyme';
import File from '../../components/File/File';

describe('<File />', () => {
	test('Should render without error', () => {
		const component = shallow(<File />);

		expect(component).toMatchSnapshot();
	});
	test('Should call onChange', () => {
		const onChange = jest.fn();
		const component = shallow(<File onChange={onChange} />);
		component.find('input').simulate('change');
		expect(onChange).toHaveBeenCalled();
	});
});
