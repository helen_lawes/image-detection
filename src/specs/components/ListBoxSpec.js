import React from 'react';
import { shallow } from 'enzyme';
import ListBox from '../../components/ListBox/ListBox';

describe('<ListBox />', () => {
	test('Should render without error', () => {
		const component = shallow(<ListBox />);

		expect(component).toMatchSnapshot();
	});
});
