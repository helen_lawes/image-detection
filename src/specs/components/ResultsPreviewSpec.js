import React from 'react';
import { shallow } from 'enzyme';
import ResultsPreview from '../../components/ResultsPreview/ResultsPreview';

describe('<ResultsPreview />', () => {
	test('Should render without error', () => {
		const component = shallow(<ResultsPreview />);

		expect(component).toMatchSnapshot();
	});
});
