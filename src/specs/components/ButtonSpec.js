import React from 'react';
import { shallow } from 'enzyme';
import Button from '../../components/Button/Button';

describe('<Button />', () => {
	test('Should render without error', () => {
		const component = shallow(<Button />);

		expect(component).toMatchSnapshot();
	});
});
