import React from 'react';
import { shallow } from 'enzyme';
import Text from '../../components/Text/Text';

describe('<Text />', () => {
	test('Should render without error', () => {
		const component = shallow(<Text />);

		expect(component).toMatchSnapshot();
	});
});
