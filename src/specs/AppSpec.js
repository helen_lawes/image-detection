import React from 'react';
import { shallow } from 'enzyme';
import App, { loadImage, getResults, uploadImage } from '../App';

jest.mock('../api', () => ({
	fetch: jest.fn().mockResolvedValue([]),
}));

describe('<App />', () => {
	test('Should render without error', () => {
		const component = shallow(<App />);

		expect(component).toMatchSnapshot();
	});
});

window.FileReader = jest.fn().mockImplementation(() => {
	return {
		addEventListener: jest.fn((_, eventHandler) =>
			eventHandler({ target: { result: 'food-label.jpg' } }),
		),
		readAsDataUrl: jest.fn(),
	};
});

window.Image = jest.fn().mockImplementation(() => {
	return {
		addEventListener: jest.fn((_, eventHandler) => eventHandler()),
	};
});

describe('loadImage', () => {
	test('It should return a file and image', async () => {
		const file = {
			lastModified: 1565692014291,
			lastModifiedDate:
				'Tue Aug 13 2019 11:26:54 GMT+0100 (British Summer Time)',
			name: 'food-label.jpg',
			size: 383482,
			type: 'image/jpeg',
		};
		const img = {
			src: 'food-label.jpg',
		};
		await expect(loadImage(file)).resolves.toMatchObject({ file, img });
	});
});

describe('getResults', () => {
	test('Should call setLoading twice', async () => {
		const setLoading = jest.fn();
		await getResults('file', () => {}, setLoading)();
		expect(setLoading).toHaveBeenCalledTimes(2);
	});
	test('Should call setResults with the results', async () => {
		const setResults = jest.fn();
		await getResults('file', setResults, () => {})();
		expect(setResults).toHaveBeenCalledWith([]);
	});
});

describe('uploadImage', () => {
	test('Should call setImage', async () => {
		const setImage = jest.fn();
		await uploadImage(setImage)({ target: { files: [] } });
		expect(setImage).toHaveBeenCalled();
	});
	test('Should call onComplete', async () => {
		const onComplete = jest.fn();
		await uploadImage(() => {}, onComplete)({ target: { files: [] } });
		expect(onComplete).toHaveBeenCalled();
	});
});
