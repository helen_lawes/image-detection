import React, { useState } from 'react';
import api from './api';

import StyledApp, { GlobalStyle, Loading } from './App.styles';
import File from './components/File/File';
import Button from './components/Button/Button';
import ResultsPreview from './components/ResultsPreview/ResultsPreview';
import ListBox from './components/ListBox/ListBox';

export const loadImage = file => {
	return new Promise(resolve => {
		const reader = new FileReader();
		reader.addEventListener('load', e => {
			const img = new Image();
			img.addEventListener('load', () => {
				resolve({ file, img });
			});
			img.src = e.target.result;
		});
		reader.readAsDataURL(file);
	});
};

export const getResults = (file, setResults, setLoading) => () => {
	setLoading(true);
	return api.fetch(file).then(results => {
		setLoading(false);
		setResults(results);
	});
};

export const uploadImage = (setImage, onComplete = () => {}) => e => {
	return loadImage(e.target.files[0]).then(image => {
		setImage(image);
		onComplete();
	});
};

const App = () => {
	const [results, setResults] = useState([]);
	const [loading, setLoading] = useState(false);
	const [image, setImage] = useState(null);
	const [find, setFind] = useState([]);
	const resetStates = () => {
		setResults([]);
	};
	return (
		<StyledApp>
			<GlobalStyle />
			<h1>Text OCR</h1>
			<File
				onChange={uploadImage(setImage, resetStates)}
				label="Choose image"
			/>
			{image && !loading && !results.length && (
				<Button
					onClick={getResults(image.file, setResults, setLoading)}>
					Extract text from image
				</Button>
			)}
			{loading && (
				<Loading>Analysing text in image, please wait...</Loading>
			)}
			{results && results.length > 0 && (
				<ListBox list={find} onUpdate={setFind} />
			)}
			{((find && find.length > 0) || image) && (
				<ResultsPreview
					results={results}
					find={find}
					image={image.img}
				/>
			)}
		</StyledApp>
	);
};

export default App;
