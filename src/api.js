class Api {
	constructor() {
		this.baseUrl = 'http://localhost:3456';
	}

	fetch(file) {
		return fetch(`${this.baseUrl}`, {
			method: 'post',
			body: this._parseData(file),
		}).then(response => this._convertResponse(response));
	}

	_parseData(file) {
		var formData = new FormData();
		formData.append('file', file);
		return formData;
	}

	_convertResponse(response) {
		if (!response.ok) throw response;
		if (typeof response.json === 'function') return response.json();
		return response;
	}
}

export default new Api();
