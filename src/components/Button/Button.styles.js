import styled from 'styled-components';
import { rem, colours } from '../../utils/styles';

const StyledButton = styled.button`
	background: ${colours.primary};
	color: ${colours.light};
	border: none;
	padding: ${rem(10)} ${rem(15)};
	border-radius: ${rem(4)};
	font-size: inherit;
`;

export default StyledButton;
