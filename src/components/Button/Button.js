import React from 'react';
import PropTypes from 'prop-types';

import StyledButton from './Button.styles';

const Button = ({ children, ...otherProps }) => (
	<StyledButton {...otherProps}>{children}</StyledButton>
);

Button.description = `
	Button component
`;

Button.propTypes = {
	children: PropTypes.node,
};

export default Button;
