import styled from 'styled-components';
import { rem, colours } from '../../utils/styles';

const StyledListBox = styled.div``;

export default StyledListBox;

export const Entry = styled.div`
	display: flex;
	align-items: center;
	margin: ${rem(20)} 0;
	> :nth-child(n + 2) {
		margin-left: ${rem(15)};
	}
`;

export const IconButton = styled.button`
	background: none;
	border: none;
	border-left: 1px solid ${colours.light};
	color: currentColor;
	margin-left: ${rem(10)};
`;

export const List = styled.ul`
	display: flex;
	padding: 0;
	li {
		display: block;
		background: ${colours.primary};
		padding: ${rem(5)} ${rem(5)} ${rem(5)} ${rem(7)};
		color: ${colours.light};
		border-radius: 4px;
		margin: 0 ${rem(5)} 0 0;
	}
`;
