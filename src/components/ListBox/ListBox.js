import React, { useState } from 'react';
import PropTypes from 'prop-types';

import StyledListBox, { Entry, IconButton, List } from './ListBox.styles';
import Text from '../Text/Text';
import Button from '../Button/Button';

const ListBox = ({ list, onUpdate }) => {
	const [text, setText] = useState('');
	const onChange = e => {
		setText(e.target.value);
	};
	const addItem = () => {
		if (!text) return;
		onUpdate([...list, text]);
		setText('');
	};
	const removeItem = item => () => {
		onUpdate([...list.filter(value => value !== item)]);
	};
	return (
		<StyledListBox>
			<Entry>
				<Text
					label="Word to highlight"
					id="text-find"
					value={text}
					onChange={onChange}
				/>
				<Button onClick={addItem}>Add</Button>
			</Entry>
			<List>
				{list.map((item, i) => (
					<li key={i}>
						{item}
						<IconButton onClick={removeItem(item)}>X</IconButton>
					</li>
				))}
			</List>
		</StyledListBox>
	);
};

ListBox.description = `
	ListBox component
`;

ListBox.propTypes = {
	list: PropTypes.arrayOf(PropTypes.string),
	onUpdate: PropTypes.func,
};

ListBox.defaultProps = {
	list: [],
	onUpdate: () => {},
};

export default ListBox;
