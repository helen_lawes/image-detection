import styled from 'styled-components';
import { rem } from '../../utils/styles';

const StyledFile = styled.div`
	position: relative;
	overflow: hidden;
	display: inline-block;
	vertical-align: middle;
	margin-right: ${rem(20)};
	input[type='file'] {
		font-size: ${rem(100)};
		position: absolute;
		top: 0;
		left: 0;
		opacity: 0;
	}
`;

export default StyledFile;
