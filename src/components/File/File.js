import React from 'react';
import PropTypes from 'prop-types';

import StyledFile from './File.styles';
import Button from '../Button/Button';

const File = ({ onChange, label }) => (
	<StyledFile>
		<Button>{label}</Button>
		<input type="file" onChange={onChange} />
	</StyledFile>
);

File.description = `
	File component
`;

File.propTypes = {
	onChange: PropTypes.func,
	label: PropTypes.string,
};

File.defaultProps = {
	onChange: () => {},
};

export default File;
