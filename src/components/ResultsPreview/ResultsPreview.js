import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import StyledResultsPreview from './ResultsPreview.styles';
import { colours } from '../../utils/styles';

const textMatch = (description, find) =>
	find.filter(item => new RegExp(item, 'ig').test(description)).length;

const drawPolygon = (ctx, vertices) => {
	ctx.beginPath();
	ctx.strokeStyle = colours.primary;
	ctx.lineWidth = 4;
	const r = 4;

	const [p1, p2, p3, p4] = vertices;
	// rounded corners
	ctx.moveTo(p1.x + r, p1.y - r);
	ctx.lineTo(p2.x - r, p2.y - r);
	ctx.arcTo(p2.x + r, p2.y - r, p2.x + r, p2.y + r, r);
	ctx.lineTo(p3.x + r, p3.y - r);
	ctx.arcTo(p3.x + r, p3.y + r, p3.x - r, p3.y + r, r);
	ctx.lineTo(p4.x + r, p4.y + r);
	ctx.arcTo(p4.x - r, p4.y + r, p4.x - r, p4.y - r, r);
	ctx.lineTo(p1.x - r, p1.y + r);
	ctx.arcTo(p1.x - r, p1.y - r, p1.x + r, p1.y - r, r);
	ctx.closePath();
	ctx.stroke();
};

const scaleVertices = (vertices, scale) =>
	vertices.map(({ x, y }) => ({ x: x * scale, y: y * scale }));

const drawCanvas = (canvasRef, previewRef, image, results, find) => {
	const canvas = canvasRef.current;
	const ctx = canvas.getContext('2d');
	canvas.width = Math.min(previewRef.current.clientWidth, image.width);
	const scale = canvas.width / image.width;
	canvas.height = scale * image.height;
	ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
	results.slice(1).forEach(({ description, boundingPoly }) => {
		if (textMatch(description, find)) {
			const { vertices } = boundingPoly;
			drawPolygon(ctx, scaleVertices(vertices, scale));
		}
	});
};

const ResultsPreview = ({ results, image, find }) => {
	const previewRef = useRef(null);
	const canvasRef = useRef(null);
	useEffect(() => {
		drawCanvas(canvasRef, previewRef, image, results, find);
	}, [results, image, find]);
	return (
		<StyledResultsPreview ref={previewRef}>
			<canvas ref={canvasRef} />
		</StyledResultsPreview>
	);
};

ResultsPreview.description = `
	ResultsPreview component
`;

ResultsPreview.propTypes = {
	results: PropTypes.array,
	image: PropTypes.any,
	find: PropTypes.array,
};

export default ResultsPreview;
