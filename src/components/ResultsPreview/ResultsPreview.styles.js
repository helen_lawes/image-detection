import styled from 'styled-components';
import { rem } from '../../utils/styles';

const StyledResultsPreview = styled.div`
	max-width: ${rem(800)};
	margin-top: ${rem(40)};
`;

export default StyledResultsPreview;
