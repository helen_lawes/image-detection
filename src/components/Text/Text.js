import React from 'react';
import PropTypes from 'prop-types';

import StyledText, { Label } from './Text.styles';

const Text = ({ label, id, ...otherProps }) => (
	<StyledText>
		<label htmlFor={id}>
			<Label>{label}</Label>
			<input id={id} type="text" {...otherProps} />
		</label>
	</StyledText>
);

Text.description = `
	Text component
`;

Text.propTypes = {
	label: PropTypes.string,
	id: PropTypes.string,
};

export default Text;
