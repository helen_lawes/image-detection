import styled from 'styled-components';
import { rem, em, colours } from '../../utils/styles';

const StyledText = styled.div`
	input {
		padding: ${rem(9)} ${rem(10)};
		border-radius: ${rem(4)};
		border: ${rem(1)} solid ${colours.mid};
		display: inline-block;
		vertical-align: middle;
		font-size: inherit;
	}
`;

export default StyledText;

export const Label = styled.span`
	margin: ${em(20)} ${rem(10)} ${em(5)} 0;
`;
