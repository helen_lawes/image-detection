import styled, { createGlobalStyle } from 'styled-components';
import { rem } from './utils/styles';

export const GlobalStyle = createGlobalStyle`
	body {
		font: 1em/normal sans-serif;
		padding: ${rem(20)};
	}
`;

const StyledApp = styled.div``;

export default StyledApp;

export const Loading = styled.div`
	margin: ${rem(20)} 0;
`;
