export const colours = {
	primary: '#2196f3',
	light: '#fff',
	mid: '#9e9e9e',
};

export const rem = (size, base = 16) => `${size / base}rem`;
export const em = (size, base = 16) => `${size / base}em`;
