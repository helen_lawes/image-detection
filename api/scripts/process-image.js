const vision = require('@google-cloud/vision');
const fs = require('fs');

const processImage = async imageFile => {
	const client = new vision.ImageAnnotatorClient();
	const fileName = imageFile.path;
	const [result] = await client.textDetection(fileName);
	// cleanup uploaded file
	fs.unlinkSync(fileName);
	return result.textAnnotations;
};

module.exports = processImage;
