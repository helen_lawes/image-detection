require('dotenv').config();
const express = require('express');
const multiparser = require('./helpers/multiparser');
const bodyParser = require('body-parser');
const processImage = require('./scripts/process-image');

const app = express();

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header(
		'Access-Control-Allow-Headers',
		'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
	);
	next();
});

app.use(multiparser());
app.use(bodyParser.json());

app.use('/', async (req, res) => {
	const { file } = req.body || {};
	const data = file ? await processImage(file) : [];
	res.send(data);
});

app.listen(process.env.PORT || 3456);
