module.exports = {
	testRegex: '/src/.*?(Spec)\\.js$',
	modulePathIgnorePatterns: ['node_modules', 'dist'],
	setupFiles: ['<rootDir>/src/specs/index.js'],
	snapshotSerializers: ['enzyme-to-json/serializer'],
};
