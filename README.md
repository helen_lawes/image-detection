# Image text detection

This application allows uploading of an image to process the text using google vision api. All the text detected is returned and then within the application a user can search for terms to find within the image.

This includes react, styled-components, eslint, prettier and parcel. Parcel is used as an application bundler.

## Prerequisites

Follow the steps in the 'before you begin' section of this page: https://cloud.google.com/vision/docs/quickstart-client-libraries
Create a environment file '.env' for storing the 'GOOGLE_APPLICATION_CREDENTIALS', see '.env.example'

## Scripts

### Dev

`npm start`

Website can be found running on http://localhost:3000

### Build

`npm run build`

Output of build will be found in the dist folder
